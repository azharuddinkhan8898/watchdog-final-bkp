/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
  // Application Constructor
  initialize: function() {
    this.bindEvents();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicitly call 'app.receivedEvent(...);'
  onDeviceReady: function() {
    console.log('Received Device Ready Event');
    console.log('calling setup push');
    // cordova.plugins.autoStart.enable();
    // cordova.plugins.backgroundMode.enable();
    // cordova.plugins.backgroundMode.overrideBackButton();
    // cordova.plugins.backgroundMode.setDefaults({ silent: true });
    // cordova.plugins.backgroundMode.excludeFromTaskList();
    // app.setupPush();
    $(function() {
      var deviceUUID = device.uuid;
      var number;
      var firstdata = false;
      var dataObj = [];
      // if (window.localStorage.getItem("websqlcreated")) {
      //     updateDataWeb();
      // }
      var db;
      var request = window.indexedDB.open("Database2", 2);

      request.onerror = function(event) {
        console.log("error: ");
      };

      request.onsuccess = function(event) {
        db = request.result;
        console.log("success: " + db);
        readAll();
      };

      request.onupgradeneeded = function(event) {
        var db = event.target.result;
        var objectStore = db.createObjectStore("records", { keyPath: "id" });
        objectStore.createIndex("datatime", "datatime");


      }

      function deleteAllWithAdd(datetime, isOnline) {
        var objectStore = db.transaction(["records"], "readwrite").objectStore("records").clear();
        objectStore.onsuccess = function(event) {
          var request = db.transaction(["records"], "readwrite").objectStore("records").add({ id: new Date().valueOf(), datatime: datetime, status: isOnline });

          request.onsuccess = function(event) {};
          request.onerror = function(event) {}
        };
      }

      function deleteAll() {
        var objectStore = db.transaction(["records"], "readwrite").objectStore("records").clear();
      }

      function readAll() {
        var objectStore = db.transaction("records").objectStore("records");
        var index = objectStore.index('datatime');
        index.openCursor().onsuccess = function(event) {
          var cursor = event.target.result;

          if (cursor) {
            console.log(cursor.value.datatime);
            var onlinetime;
            var offlinetime;
            var onlineDate;
            var offlineDate;
            var lastDataGroup = null;
            // var dataObjData = { datetime: cursor.value.datatime, status: cursor.value.status };
            // dataObj.push(dataObjData);
            //console.log(dataObj);
            //console.log(cursor.value.datatime.split(" ")[0].replace(",", ""))
            var dataArray = cursor.value.datatime.split(" ");
            var dataDate = dataArray[0].replace(",", "");
            var dataTime = dataArray[1] + " " + dataArray[2];
            if (cursor.value.status == true) {
              onlineDate = dataDate;
              if (dataDate != lastDataGroup) {
                $(".onlineData").prepend("<div class='data-group' data-date='" + dataDate + "'><div class='group-heading'>" + huminized(dataDate) + "</div></div>");
              }
              var targetnametemp = window.localStorage.getItem("targetName");
              // cordova.plugins.notification.local.schedule({
              //     title: targetnametemp + " is now online.",
              //     // text: 'Thats pretty easy...',
              //     foreground: true
              // });

              $(".wait-for-user").hide();
              $(".onlineData > .data-group[data-date='" + dataDate + "'] .group-heading").after('<div class="data"> <div class="online-details"><span class="date-time"></span> </div><div class="online-duration"></div><div class="offline-details"> <div class="current-online">Online</div> <div class="offline-time"><span class="date-time"></span></div> </div> </div><div class="clearfix"></div>');
              $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .online-details .date-time").append(dataTime);
              $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .current-online").show();
              //clearInterval(updatelastseenInt);
              $("#lastSeen").empty().append("Online");
              onlinetime = cursor.value.datatime + "";
              if (updatelastseenInt) { clearInterval(updatelastseenInt) };
            } else {
              // insertData(cursor.value.datatime, cursor.value.status);
              // firstdata = true;
              if (onlineDate) {
                offlineDate = dataDate;
                if (dataDate != lastDataGroup) {
                  $(".onlineData").prepend("<div class='data-group' data-date='" + dataDate + "'><div class='group-heading'>" + huminized(dataDate) + "</div></div>");
                }
                if (onlineDate != offlineDate) {
                  $(".onlineData > .data-group[data-date='" + dataDate + "'] .group-heading").after('<div class="data"> <div class="online-details"><span class="date-time">--</span> </div><div class="online-duration"></div><div class="offline-details"> <div class="current-online">Online</div> <div class="offline-time"><span class="date-time"></span></div> </div> </div><div class="clearfix"></div>');
                  $(".onlineData > .data-group[data-date='" + onlineDate + "'] div.data:nth-child(2) .offline-details .current-online").empty().append("--");

                }
                $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .offline-time").show();
                $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .current-online").hide();
                $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .date-time").append(dataTime);
                lastSeenDate = dataDate;
                lastSeenTime = dataTime;
                // var onlineTime =
                updatelastseen();

                $("#lastSeen").empty().append(huminized(dataDate) + ", " + dataTime);
                offlinetime = cursor.value.datatime + "";
                $(".onlineData > .data-group[data-date='" + dataDate + "'] .data:nth-child(2) .online-duration").append(calculateOnlineTime(onlinetime, offlinetime))
              }


            }
            lastDataGroup = dataDate;
            console.log(cursor.value)
            cursor.continue();
          } else {

          }
        };
      }


      function updateToken() {
        FCMPlugin.getToken(function(token) {
          // alert(token);
          if (window.localStorage.getItem("trackid")) {
            numbers.on('value', function(snapshot) {
              snapshot.forEach(function(childSnapshot) {
                if (childSnapshot.val().id == window.localStorage.getItem("trackid"))
                  childSnapshot.ref.update({ deviceToken: token });
              });
            });
          }
        });

      }

      function insertData(datetime, isOnline) {
        var request = db.transaction(["records"], "readwrite").objectStore("records");
        if (firstdata == false) {
          console.log("deleted")
          deleteAllWithAdd(datetime, isOnline);
          // request.add({ id: new Date().valueOf(), datatime: datetime, status: isOnline });

          // request.onsuccess = function(event) {};

          // request.onerror = function(event) {}
          // db.query('INSERT INTO newrecords (datatime, status) VALUES (?,?)', [
          //   [datetime, isOnline]
          // ]);
        } else {
          console.log("deleted not deleted")
          request.add({ id: new Date().valueOf(), datatime: datetime, status: isOnline });
          // db.query('SELECT * FROM newrecords').fail(function(tx, err) {
          //   throw new Error(err.message);
          // }).done(function(products) {
          //   if (products.length > 0) {
          //     console.log(products)
          //   }
          // });

          // db.query('SELECT * FROM records').fail(function(tx, err) {
          //     throw new Error(err.message);
          // }).done(function(products) {

          // });
        }

      }




      // if (window.localStorage.getItem("tracknum")) {
      //   trackNumFound();
      // } else {
      //   $("#form").show();
      // }
      if (window.localStorage.getItem("trackid") && window.localStorage.getItem("tracknum")) {
        // lastDataGroup = null;
        // lastDataGroup = null;
        fetchData();
        trackNumFound();
        updateToken();

      } else {

        // deviceUUID = device.uuid;
        var foundUUID = false;
        //console.log(deviceUUID)
        $("#form").hide();
        $("#loader").show();
        $("#trackingData").hide();
        numbers.on('value', function(snapshot) {
          snapshot.forEach(function(childSnapshot) {

            if (childSnapshot.val().UUID == deviceUUID) {
              window.localStorage.setItem("tracknum", childSnapshot.val().MobileNumber);
              window.localStorage.setItem("trackid", childSnapshot.val().id);
              window.localStorage.setItem("targetName", childSnapshot.val().targetName);
              // lastDataGroup = null;
              fetchData();
              trackNumFound();
              updateToken();

              foundUUID = true;
            }
          });

          if (foundUUID == false) {
            $("#form").show();
            $("#loader").hide();
            $("#trackingData").hide();
          }
        });
        //numbers.off();

      }
      $("#btn_change").click(function() {
        var confirmm = confirm("This will erase all the tracking data for this number.");
        if (confirmm == true) {


        }

      });
      $("#btn_submit").click(function() {

        if (isEverythingFilled()) {
          $("#loader").show();
          incId();
        }


      });
      $("#form input, #form select").on("change keyup paste", function() {
        $(this).parent().find(".mdl-textfield__error").css("visibility", "hidden");
      })

      function isEverythingFilled() {
        if (!($("#targetName").val())) {
          $("#targetName").parent().find(".mdl-textfield__error").css("visibility", "visible");
        } else {

          if ($("#countryOptions").val() == "null") {
            $("#countryOptions").parent().find(".mdl-textfield__error").css("visibility", "visible");
          } else {
            if (!($("#NUMBER").val())) {
              $("#NUMBER").parent().find(".mdl-textfield__error").css("visibility", "visible");
            } else {

              return true;

            }
          }
        }
      }

      function removeData() {
        if (window.localStorage.getItem("tracknum") && window.localStorage.getItem("trackid")) {
          $("#loader").show();
          var tracknid = window.localStorage.getItem("trackid");
          console.log(tracknid)
          //tracknid = tracknid.toString();
          numbers.on('value', function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
              var childData = childSnapshot.val();
              if (childData.id == tracknid) {
                childSnapshot.ref.remove();
              }
            });
          });
          users.child(tracknid).remove(function() {
            $("#loader").hide();
            $("#form").show();
            $("#trackingData").hide();
            window.localStorage.removeItem("tracknum");
            window.localStorage.removeItem("trackid");
            $(".onlineData").empty();
            $(".wait-for-user").show();
            var db = WebSQL('mydb');
            db.query('DELETE FROM records');
            firstdata = false;
            lastDataGroup = null;
            deleteAll();
          });
        }
      }

      function incId() {
        // increment the counter
        counterID.transaction(function(currentValue) {
          return (currentValue || 0) + 1
        }, function(err, committed, ss) {
          if (err) {
            setError(err);
          } else if (committed) {
            // if counter update succeeds, then create record
            // probably want a recourse for failures too
            addRecord(ss.val());
          }
        });
      }

      function addRecord(id) {
        setTimeout(function() {
          countryCode = $("#countryOptions").val();
          numberWitoutCode = $("#NUMBER").val();
          targetName = $("#targetName").val();
          number = "" + countryCode + numberWitoutCode;
          //deviceUUID = device.uuid;
          numbers.push({
            id: id,
            MobileNumber: number,
            UUID: deviceUUID,
            targetName: targetName
          }, function() {
            console.log(number);
            window.localStorage.setItem("tracknum", number);
            window.localStorage.setItem("trackid", id);
            window.localStorage.setItem("targetName", targetName);
            var idd = id;
            lastDataGroup = null;
            trackNumFound();
            fetchData();
            updateToken();

            users.child(idd).set({
              MobileNumber: number
            })
          });

          // fb.child('records').child('rec' + id).set('record #' + id, function(err) {
          //     err && setError(err);
          // });
        });

      }

      // _.observe(dataObj, 'create', function(new_item, item_index) {
      //   console.log(new_item);
      // });
      var lastSeenDate;
      var lastSeenTime;
      var firstLiveData = false;
      var updatelastseenInt;

      function fetchData() {
        var onlinetime;
        var offlinetime;
        var onlineDate;
        var offlineDate;



        var lastDataGroup = null;
        console.log("called");
        numbers.off();
        users.child(window.localStorage.getItem("trackid")).child("records").off();
        users.child(window.localStorage.getItem("trackid")).child("records").on("child_added", function(data, prevChildKey) {

          if (firstLiveData == false) {
            $(".onlineData").empty();
            firstLiveData = true;
          }
          insertData(data.val().datatime, data.val().status);
          firstdata = true;
          // var dataObjData = { datetime: data.val().datatime, status: data.val().status };
          // dataObj.push(dataObjData);
          //console.log(dataObj);
          //console.log(data.val().datatime.split(" ")[0].replace(",", ""))
          var dataArray = data.val().datatime.split(" ");
          var dataDate = dataArray[0].replace(",", "");
          var dataTime = dataArray[1] + " " + dataArray[2];
          if (data.val().status == true) {
            onlineDate = dataDate;
            if (dataDate != lastDataGroup) {
              $(".onlineData").prepend("<div class='data-group' data-date='" + dataDate + "'><div class='group-heading'>" + huminized(dataDate) + "</div></div>");
            }
            var targetnametemp = window.localStorage.getItem("targetName");
            // cordova.plugins.notification.local.schedule({
            //     title: targetnametemp + " is now online.",
            //     // text: 'Thats pretty easy...',
            //     foreground: true
            // });

            $(".wait-for-user").hide();
            $(".onlineData > .data-group[data-date='" + dataDate + "'] .group-heading").after('<div class="data"> <div class="online-details"><span class="date-time"></span> </div><div class="online-duration"></div><div class="offline-details"> <div class="current-online">Online</div> <div class="offline-time"><span class="date-time"></span></div> </div> </div><div class="clearfix"></div>');
            $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .online-details .date-time").append(dataTime);
            $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .current-online").show();
            //clearInterval(updatelastseenInt);
            $("#lastSeen").empty().append("Online");
            onlinetime = data.val().datatime + "";
            if (updatelastseenInt) { clearInterval(updatelastseenInt) };
          } else {
            // insertData(data.val().datatime, data.val().status);
            // firstdata = true;
            if (onlineDate) {
              offlineDate = dataDate;
              if (dataDate != lastDataGroup) {
                $(".onlineData").prepend("<div class='data-group' data-date='" + dataDate + "'><div class='group-heading'>" + huminized(dataDate) + "</div></div>");
              }
              if (onlineDate != offlineDate) {
                $(".onlineData > .data-group[data-date='" + dataDate + "'] .group-heading").after('<div class="data"> <div class="online-details"><span class="date-time">--</span> </div><div class="online-duration"></div><div class="offline-details"> <div class="current-online">Online</div> <div class="offline-time"><span class="date-time"></span></div> </div> </div><div class="clearfix"></div>');
                $(".onlineData > .data-group[data-date='" + onlineDate + "'] div.data:nth-child(2) .offline-details .current-online").empty().append("--");

              }
              $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .offline-time").show();
              $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .current-online").hide();
              $(".onlineData > .data-group[data-date='" + dataDate + "'] div.data:nth-child(2) .offline-details .date-time").append(dataTime);
              lastSeenDate = dataDate;
              lastSeenTime = dataTime;
              // var onlineTime =
              updatelastseen();

              $("#lastSeen").empty().append(huminized(dataDate) + ", " + dataTime);
              offlinetime = data.val().datatime + "";
              $(".onlineData > .data-group[data-date='" + dataDate + "'] .data:nth-child(2) .online-duration").append(calculateOnlineTime(onlinetime, offlinetime))
            }


          }
          lastDataGroup = dataDate;
          console.log(data.val())
        });
      }

      function updatelastseen() {
        updatelastseenInt = setInterval(function() {
          if (lastSeenDate) {
            $("#lastSeen").empty().append(huminized(lastSeenDate) + ", " + lastSeenTime);
          }
        }, 1000);
      }

      // setInterval(function(){
      // 	var groupheadingL = $(".group-heading").length;
      // 	for(i=0;i<groupheadingL;i++){
      // 		var dataDate = $(".groupx-heading").eq(i).text();
      // 		$(".group-heading").eq(i).empty().append(huminized(dataDate));
      // 	}
      // },1000)


      function trackNumFound() {
        $("#form").hide();
        $("#loader").hide();
        $("#trackingData").show();
        $("#targetNameDisplay").empty().append(window.localStorage.getItem("targetName"));
        $(".tracking-text .tracking-num").empty().append(window.localStorage.getItem("tracknum"));
      }
      // setInterval(function() {
      //   $.ajax({
      //     url: "http://azharuddin8898.0fees.us/whatseen/getnumbers.php",
      //     type: "GET",
      //     dataType: 'jsonp',
      //     success: function(data) {

      //       console.log(data)
      //       // $.each(obj, function(index, value){
      //       //   alert(index + " : " + value);
      //       // })
      //     }

      //   });
      // }, 1000);
      var dialogButton = document.querySelector('.dialog-button');
      var dialog = document.querySelector('#dialog');
      if (!dialog.showModal) {
        dialogPolyfill.registerDialog(dialog);
      }
      dialogButton.addEventListener('click', function() {
        dialog.showModal();
      });
      dialog.querySelector('#closeNo')
        .addEventListener('click', function() {
          dialog.close();
        });
      dialog.querySelector('#closeYes')
        .addEventListener('click', function() {
          removeData();
          dialog.close();
        });

      function calculateOnlineTime(onlineTime, offlineTime) {


        var startTime = moment(onlineTime, "MM/DD/YYYY, HH:mm:ss A");
        var endTime = moment(offlineTime, "MM/DD/YYYY, HH:mm:ss A");
        var duration = moment.duration(endTime.diff(startTime));
        var hours = parseInt(duration.asHours());
        var minutes = parseInt(duration.asMinutes()) - hours * 60;
        var seconds = parseInt(duration.asSeconds()) - minutes * 60 - hours * 60 * 60;
        var onlineDiration = "";
        if (hours != 0) {
          onlineDiration += hours + (hours > 1 ? (minutes != 0 ? " hours, " : " hours and ") : (minutes != 0 ? " hour, " : " hour and "))
        }
        if (minutes != 0) {
          onlineDiration += minutes + (minutes > 1 ? (seconds != 0 ? " minutes and " : " minutes") : (seconds != 0 ? " minute and " : " minute"))
        }

        if (seconds != 0) {
          onlineDiration += "" + seconds + (seconds > 1 ? " seconds" : " second")
        }
        return onlineDiration;
      }

      function huminized(date) {
        var date = moment(date, "MM/DD/YYYY");
        return date.calendar(null, { sameDay: '[Today]', nextDay: '[Tomorrow]', nextWeek: 'dddd', lastDay: '[Yesterday]', lastWeek: 'DD MMMM, YYYY', sameElse: 'DD MMMM, YYYY' });
      }
    });

  },
  setupPush: function() {
    console.log('calling push init');
    var push = PushNotification.init({
      "android": {
        "senderID": "XXXXXXXX"
      },
      "browser": {},
      "ios": {
        "sound": true,
        "vibration": true,
        "badge": true
      },
      "windows": {}
    });
    console.log('after init');

    push.on('registration', function(data) {
      console.log('registration event: ' + data.registrationId);

      var oldRegId = window.localStorage.getItem('registrationId');
      if (oldRegId !== data.registrationId) {
        // Save new registration ID
        window.localStorage.setItem('registrationId', data.registrationId);
        // Post registrationId to your app server as the value has changed
      }

      var parentElement = document.getElementById('registration');
      var listeningElement = parentElement.querySelector('.waiting');
      var receivedElement = parentElement.querySelector('.received');

      listeningElement.setAttribute('style', 'display:none;');
      receivedElement.setAttribute('style', 'display:block;');
    });

    push.on('error', function(e) {
      console.log("push error = " + e.message);
    });

    push.on('notification', function(data) {
      console.log('notification event');
      navigator.notification.alert(
        data.message, // message
        null, // callback
        data.title, // title
        'Ok' // buttonName
      );
    });
  }
};
